# UWPAppBackup
Tool to create Install-able Backups of UWP Apps that are Already Installed.

Your system has to be in [Developer Mode](https://www.ghacks.net/2015/06/13/how-to-enable-developer-mode-in-windows-10-to-sideload-apps/) in order to install the backups created by this application.

*This is because its impossible to rebuild a store appx from the extracted files and anything i attempted resulted in the signature being invalid, and you cant sideload a Store-App from just an extracted folder.*

Download: https://bitbucket.org/SilicaAndPina/uwpappbackup/downloads/UWPAppBackup.exe