﻿namespace UWPAppBackup
{
    partial class UWPExtractor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UWPExtractor));
            this.UwpApps = new System.Windows.Forms.ListBox();
            this.AppBackup = new System.Windows.Forms.Button();
            this.BackupProgress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // UwpApps
            // 
            this.UwpApps.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UwpApps.FormattingEnabled = true;
            this.UwpApps.Location = new System.Drawing.Point(12, 12);
            this.UwpApps.Name = "UwpApps";
            this.UwpApps.Size = new System.Drawing.Size(485, 264);
            this.UwpApps.TabIndex = 0;
            // 
            // AppBackup
            // 
            this.AppBackup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AppBackup.Location = new System.Drawing.Point(12, 311);
            this.AppBackup.Name = "AppBackup";
            this.AppBackup.Size = new System.Drawing.Size(485, 28);
            this.AppBackup.TabIndex = 1;
            this.AppBackup.Text = "Backup App";
            this.AppBackup.UseVisualStyleBackColor = true;
            this.AppBackup.Click += new System.EventHandler(this.AppBackup_Click);
            // 
            // BackupProgress
            // 
            this.BackupProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BackupProgress.Location = new System.Drawing.Point(12, 282);
            this.BackupProgress.Name = "BackupProgress";
            this.BackupProgress.Size = new System.Drawing.Size(485, 23);
            this.BackupProgress.TabIndex = 2;
            // 
            // UWPExtractor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 351);
            this.Controls.Add(this.BackupProgress);
            this.Controls.Add(this.AppBackup);
            this.Controls.Add(this.UwpApps);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UWPExtractor";
            this.Text = "UWP Backup Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox UwpApps;
        private System.Windows.Forms.Button AppBackup;
        private System.Windows.Forms.ProgressBar BackupProgress;
    }
}

