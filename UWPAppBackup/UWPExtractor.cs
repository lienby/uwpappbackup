﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UWPAppBackup
{
    public partial class UWPExtractor : Form
    {
        public UWPExtractor()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String[] Dirs = Directory.GetDirectories(@"C:\Program Files\WindowsApps");
            foreach(String Dir in Dirs)
            {
                if(File.Exists(Dir + "\\AppxManifest.xml"))
                {
                    UwpApps.Items.Add(Path.GetFileName(Dir));
                }
                
            }
            
        }

        private void AppBackup_Click(object sender, EventArgs e)
        {
            String AppName = UwpApps.SelectedItem.ToString();
            String path = @"C:\Program Files\WindowsApps\" + AppName;

            String Out = "";


            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    Out = fbd.SelectedPath;
                }
            }

            if(!Directory.Exists(Out))
            {
                Directory.CreateDirectory(Out);
            }

            if (!Directory.Exists(Out + "\\" + AppName))
            {
                Directory.CreateDirectory(Out+"\\"+ AppName);
            }


            int NumberOfFiles = Directory.GetFiles(path, "*", SearchOption.AllDirectories).Length;
            BackupProgress.Maximum = NumberOfFiles;

            foreach (string dirPath in Directory.GetDirectories(path, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(path, Out + "\\" + AppName));
            }
            
            foreach (string newPath in Directory.GetFiles(path, "*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(path, Out + "\\" + AppName), true);
                BackupProgress.Increment(1);
            }

            String InstallBat = "@echo off\r\necho Please make sure to enable Developer Mode in Settings.\r\necho Press any key to install \"" + AppName + "\"\r\npause>nul\r\ncd " + AppName + "\r\nren \"AppxBlockMap.xml\" \"_AppxBlockMap.xml\r\nren \"AppxSignature.p7x\" \"_AppxSignature.p7x\r\npowershell -Command \"Add-AppxPackage -Path AppxManifest.xml -Register\r\nren \"_AppxBlockMap.xml\" \"AppxBlockMap.xml\"\r\nren \"_AppxSignature.p7x\" \"AppxSignature.p7x\"\r\necho Done, press any key to exit.\r\npause > nul";
            File.WriteAllText(Out + "\\Install.bat", InstallBat);
            MessageBox.Show("Done!","Backup Created",MessageBoxButtons.OK,MessageBoxIcon.Information);
            BackupProgress.Value = 0;


        }


        }
    }
